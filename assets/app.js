let companies = [];

// Initialization after the HTML document has been loaded...
window.addEventListener('DOMContentLoaded', () => {

    // Function calls for initial page loading activities...
    doLoadCompanies();
    setUpHeaderBar();
});

/* 
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/

async function doLogin() {
    let usernameElement = document.querySelector('#loginUsername');
    let passwordElement = document.querySelector('#loginPassword');
    let credentials = {
        username: usernameElement.value,
        password: passwordElement.value
    };
    let loginResponse = await postCredentials(credentials);
    if (loginResponse.id > 0) {
        localStorage.setItem('LOGIN_USERNAME', loginResponse.username);
        localStorage.setItem('LOGIN_TOKEN', loginResponse.token);
        setUpHeaderBar();
        closePopup();
        doLoadCompanies();
    } else {
        // Ilmnes mingi jama, lahendame selle siin...
        doLogout();
        displayLoginPopup();
    }
}

function doLogout() {
    localStorage.removeItem('LOGIN_USERNAME');
    localStorage.removeItem('LOGIN_TOKEN');
    displayLoginPopup();
}

async function doLoadCompanies() {
    console.log('Loading companies...');
    companies = await fetchCompanies();
    if (companies) {
        displayCompanies(companies);
    } else {
        displayLoginPopup();
    }
}

async function doDeleteCompany(companyId) {
    if (confirm('Soovid sa tõesti seda ettevõtet kustutada?')) {
        await removeCompany(companyId);
        await doLoadCompanies();
    }
}

/* 
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

function displayCompanies(companies) {
    const mainElement = document.querySelector('main');
    let companiesHtml = '';

    for (const company of companies) {
        companiesHtml += /*html*/`
            <div class="company-item-box">
                <div class="company-item-logo"><img src="${company.logo}" height="50"></div>
                <div class="company-item-property">Nimi: </div>
                <div class="company-item-value">${company.name}</div>
                <div class="company-item-property">Astutatud: </div>
                <div class="company-item-value">${company.established}</div>
                <div class="company-item-property">Töötajaid: </div>
                <div class="company-item-value">${company.employees}</div>
                <div class="company-item-announcements">
                    <h3>Announcements</h3>
                    ${displayAnnouncements(company.announcements, company.id)}
                </div>
                <div class="company-item-button">
                    <button class="button-red" onclick="doDeleteCompany(${company.id})">Kustuta</button>
                    <button onclick="displayCompanyEditPopup(${company.id})">Muuda</button>
                </div>
            </div>
        `;
    }

    mainElement.innerHTML = companiesHtml;
}

function displayAnnouncements(announcements, companyId) {
    let newsHtml = '';

    for (let announcement of announcements) {
        newsHtml += /*html*/`
            <div>
                <h4>${announcement.title}</h4>
                <p>${announcement.body}</p>
                <p><button onclick="displayAnnouncementPopup(${companyId}, ${announcement.id})">Muuda</button></p>
            </div>
        `;
    }

    newsHtml += /*html*/`
        <div>
            <button onclick="displayAnnouncementPopup(${companyId})">Lisa uudis</button>
        </div>
    `;

    return newsHtml;
}

async function displayLoginPopup() {
    openPopup(POPUP_CONF_BLANK_300_300, 'loginTemplate');
}

async function displayCompanyEditPopup(companyId) {
    await openPopup(POPUP_CONF_500_500, 'companyEditTemplate');

    if (companyId > 0) {
        // Muudame olemasolevat ettevõtet.
        // Täidame vormi väljad...
        const company = await fetchCompany(companyId);
        document.querySelector('#companyId').value = company.id;
        document.querySelector('#companyName').value = company.name;
        document.querySelector('#companyLogo').value = company.logo;
        document.querySelector('#companyEstablished').value = company.established;
        document.querySelector('#companyEmployees').value = company.employees;
    }
}

async function editCompany() {
    const uploadedFileUrl = await uploadLogoFile();
    if (uploadedFileUrl != null) {
        document.querySelector('#companyLogo').value = uploadedFileUrl;
    }

    const validationResult = validateCompanyForm();

    if (validationResult.length == 0) {
        let company;
        if (document.querySelector('#companyId').value > 0) {
            // Olemasoleva ettevõtte muutmine.
            company = {
                id: document.querySelector('#companyId').value,
                name: document.querySelector('#companyName').value,
                logo: document.querySelector('#companyLogo').value,
                established: document.querySelector('#companyEstablished').value,
                employees: document.querySelector('#companyEmployees').value
            };
            await putCompany(company);
        } else {
            // Uue ettevõtte lisamine.
            company = {
                name: document.querySelector('#companyName').value,
                logo: document.querySelector('#companyLogo').value,
                established: document.querySelector('#companyEstablished').value,
                employees: document.querySelector('#companyEmployees').value
            };
            await postCompany(company);
        }
        await doLoadCompanies();
        await closePopup();
    } else {
        displayCompanyFormErrors(validationResult);
    }
}

async function uploadLogoFile() {
    if (document.querySelector('#companyFile').files.length > 0) {
        const fileUploadResponse = await postFile(document.querySelector('#companyFile').files[0]);
        return fileUploadResponse.url;
    } else {
        return null;
    }
}

function validateCompanyForm() {
    let errors = [];

    let companyName = document.querySelector('#companyName').value;
    let companyLogo = document.querySelector('#companyLogo').value;
    let companyEstablished = document.querySelector('#companyEstablished').value;
    let companyEmployees = document.querySelector('#companyEmployees').value;

    if (companyName.length < 2) {
        errors.push('Ettevõtte nimi kas määramata või liiga lühike (min 2 tähemärki)!');
    }
    if (companyLogo.length < 15) {
        errors.push('Ettevõtte logo kas määramata!');
    }
    if (companyLogo.length > 15 && !companyLogo.startsWith("http")) {
        errors.push('Ettevõtte logo peab algama kas http://... või https://...!');
    }
    if (companyEstablished == '') {
        errors.push('Ettevõtte asutamisaeg kas määramata!');
    }
    if (companyEmployees < 1) {
        errors.push('Ettevõttel peab olema vähemalt üks töötaja!');
    }

    return errors;
}

function displayCompanyFormErrors(errors) {
    const errorBox = document.querySelector('#errorBox');
    errorBox.style.display = 'block';

    let errorsHtml = '';
    for (let errorMessage of errors) {
        errorsHtml += /*html*/`<div>${errorMessage}</div>`;
    }

    errorBox.innerHTML = errorsHtml;
}

function setUpHeaderBar() {
    document.querySelector('#headerBar span').textContent = localStorage.getItem('LOGIN_USERNAME');
}

function filterCompanies() {
    const searchText = document.querySelector('#searchText');

    const filteredCompanies = companies.filter(c => c.name.toLowerCase().includes(searchText.value.toLowerCase()));
    displayCompanies(filteredCompanies);
}

async function displayAnnouncementPopup(companyId, announcementId) {
    await openPopup(POPUP_CONF_500_500, 'announcementEditTemplate');

    document.querySelector('#announcementCompanyId').value = companyId;

    if (announcementId > 0) {
        const announement = companies
            .find(c => c.id === companyId)
            .announcements.find(a => a.id === announcementId);

        document.querySelector('#announcementId').value = announement.id;
        document.querySelector('#announcementTitle').value = announement.title;
        document.querySelector('#announcementBody').value = announement.body;
    }
}

async function editAnnouncement() {

    const companyId = document.querySelector('#announcementCompanyId').value;
    const announcementId = document.querySelector('#announcementId').value;
    let announcement;

    if (announcementId > 0) {
        announcement = {
            id: announcementId,
            title: document.querySelector('#announcementTitle').value,
            body: document.querySelector('#announcementBody').value
        };
        await putAnnouncement(announcement);
    } else {
        announcement = {
            title: document.querySelector('#announcementTitle').value,
            body: document.querySelector('#announcementBody').value
        };
        await postAnnouncement(announcement, companyId);
    }
    await doLoadCompanies();
    await closePopup();
}