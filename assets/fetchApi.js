
async function postCredentials(credentials) {
    try {
        const response = await fetch(`${API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        });
        return await response.json();
    } catch (e) {
        console.log('LOGIN FAILED', e);
    }
}

async function fetchCompanies() {
    try {
        const response = await fetch(`${API_URL}/companies/all`, {
            method: 'GET',
            headers: {
                'Authorization': composeBearerToken()
            }
        });

        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR OCCURRED', e);
    }
}

async function fetchCompany(companyId) {
    try {
        const response = await fetch(`${API_URL}/companies/${companyId}`, {
            method: 'GET',
            headers: {
                'Authorization': composeBearerToken()
            }
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR OCCURRED', e);
    }
}

async function removeCompany(companyId) {
    try {
        const response = await fetch(`${API_URL}/companies/${companyId}`, {
            method: 'DELETE',
            headers: {
                'Authorization': composeBearerToken()
            }
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR OCCURRED', e);
    }
}

async function postCompany(company) {
    try {
        const response = await fetch(`${API_URL}/companies/add`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': composeBearerToken()
            },
            body: JSON.stringify(company)
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR IN ADDING COMPANY', e);
    }
}

async function putCompany(company) {
    try {
        const response = await fetch(`${API_URL}/companies/update`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': composeBearerToken()
            },
            body: JSON.stringify(company)
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR IN MODIFYING COMPANY', e);
    }
}

async function postFile(file) {
    try {
        let fileForm = new FormData();
        fileForm.append('file', file);

        const response = await fetch(`${API_URL}/files/upload`, {
            method: 'POST',
            headers: {
                'Authorization': composeBearerToken()
            },
            body: fileForm
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR IN MODIFYING COMPANY', e);
    }
}

async function postAnnouncement(announcement, companyId) {
    try {
        const response = await fetch(`${API_URL}/companies/announcements/add/${companyId}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': composeBearerToken()
            },
            body: JSON.stringify(announcement)
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR IN ADDING COMPANY', e);
    }
}

async function putAnnouncement(announcement) {
    try {
        const response = await fetch(`${API_URL}/companies/announcements/update`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': composeBearerToken()
            },
            body: JSON.stringify(announcement)
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR IN ADDING COMPANY', e);
    }
}

function composeBearerToken() {
    return `Bearer ${localStorage.getItem('LOGIN_TOKEN')}`;
}

async function processProtectedResponse(response) {
    if (response.status >= 400 && response.status <= 403) {
        throw new Error('Unauthorized');
    } else {
        return await response.json();
    }
}